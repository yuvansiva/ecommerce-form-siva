
const mongoose = require('mongoose');

const ProductSchema = mongoose.Schema({
    prdctname: { type: String },
    price: { type: String },
    image: { type: String },
    createdDate: { type: Date, default: Date.now() },   


});

module.exports = mongoose.model('product', ProductSchema);