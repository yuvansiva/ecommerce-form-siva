const  express  = require('express')
const router = express.Router()
let common = require('../helper/common')
let registerDB = require('../schema/register')
let productDB = require('../schema/product')


var multer = require('multer');
var cloudinary = require('cloudinary');


cloudinary.config({
    cloud_name: 'dltvddllp',
    api_key: '157367412622144',
    api_secret: 'R1zUjDXTDKGEqkBokssKrzfVtrU'
});



var storage = multer.diskStorage({
    filename: function (req, file, cb) {
        cb(null, file.originalname);
    }
})

var upload = multer({ storage: storage });
router.post("/upload", upload.array("uploads[]", 12), function (req, res) {
    if (req.files.length > 0) {
        cloudinary.uploader.upload(req.files[0].path, function (result) {
            res.json({ "status": true, "result": result.secure_url });
        });
        console.log('result.secure_url')
    } else {
        res.json({ "status": false, "Message": "Please upload valid file!" });
        console.log('dfjksdaf')
    }
});



router.post('/register', (req,res)=> {

    var obj = {
        email : common.encrypt(req.body.email),
        Username : req.body.Username,
        password : common.encrypt(req.body.password)
    }
    registerDB.create(obj, (err,data) => {
        if(err){
            res.json({
                status:false,Message:"please try again later"
            })
        }
        else{
            res.json({
                status:true,Message:"Register successfully"
            }) 
        }
    })
})



router.post('/login', (req,res)=> {
    registerDB.findOne({email : common.encrypt(req.body.email)}).exec((err,data)=> {
        if(err){
            res.json({
                status:false,Message:"Authentication failed."
            }) 
        }
        else if(data == undefined){
            res.json({
                status:false,Message:"User not found"
            }) 
        }
        else if (data){
            if(data.password != common.encrypt(req.body.password))
            {
                res.json({
                    status:false,Message:'Authentication failed. Wrong password.'
                }) 
            }
            else{
                res.json({
                    status:true,Message:'Login Successfully'
                }) 
            }
        }

    })
})


router.post('/addproduct', (req,res)=> {

         if(req.body._id == '')
         {
            var obj = {
                prdctname : req.body.prdctname,
                price : req.body.price,
                image : req.body.image
            }
            productDB.create(obj, (err,data) => {
                if(err){
                    res.json({
                        status:false,Message:"please try again later"
                    })
                }
                else{
                    res.json({
                        status:true,Message:"Product Added successfully"
                    }) 
                }
            })
         }
        else {
            // productDB.find({_id:req.body._id}).exec((err,data)=> {
                // if(err){
                //     res.json({
                //         status:false,Message:"please try again later"
                //     })
                // }
                // else{
                    productDB.updateOne({_id:req.body._id}, {$set: {
                        prdctname : req.body.prdctname,
                        price : req.body.price,
                        image : req.body.image
                    }}).exec((err,data)=> {
                        if(err){
                            res.json({
                                status:false,Message:"please try again later"
                            })
                        }
                        else{
                            res.json({
                                status:true,Message:"product updated successfully"
                            })
                        }
                    })
                // }
            // })
        }
         

  
});


router.get('/getproduct', (req,res)=> {
    productDB.find().exec((err,data)=> {
        if(err){
            console.log('err')
        }
        else{
            res.json({
                status:false,Message:data
            })
        }
    })
})


router.post('/edit', (req,res)=> {
    productDB.find({_id:req.body._id}).exec((err,data)=> {
        if(err){
            console.log('err')
        }
        else{

            array = [];
            for(var i=0; i<data.length; i++){
                var obj = {
                    
                    _id:data[i]._id,
                    prdctname : data[i].prdctname,
                    price : data[i].price,
                    image : data[i].image
                }
                array.push(obj)
            }
            res.json({
                status:true,Message:array
            })
            console.log(array)
        }
    })
})

router.post('/delete', (req,res) => {
    productDB.deleteOne({_id:req.body._id}).exec((err,data)=> {
        if(err){
            res.json({
                status:false,Message:'please try again later'
            })
        }
        else{
            res.json({
                status:true,Message:'Deleted successfully'
            }) 
        }
    })
});

router.get('/getproducts', (req,res)=> {
    productDB.find().exec((err,data)=> {
        if(err){
            res.json({
                status:false,Message:"please try again later"
            })
        }
        else{
            res.json({
                status:true,Message:data
            })
        }
    })
});

module.exports = router