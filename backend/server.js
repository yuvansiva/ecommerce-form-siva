const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');
const mongoose = require('./DB/db')


const adminapi = require('./routes/admin')



const PORT = process.env.PORT || 4005;
const IP = "IP";

var app = express();
app.use(bodyParser.json());
app.use(cors());

app.use('/adminapi', adminapi);


app.listen(PORT, IP, () => {
    console.log('server conected with host' + ' ' + IP + ' ' + 'and port' + ' ' + PORT)
});