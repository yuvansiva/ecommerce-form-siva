import { Component } from '@angular/core';
import { MainService } from '../service/main.service';
import { ToastrManager } from 'ng6-toastr-notifications';
import { Router} from '@angular/router'

@Component({
  selector: 'app-dashboard',
  templateUrl: 'register.component.html'
})
export class RegisterComponent {
   model : any = {}
  constructor(private api: MainService, private toastr: ToastrManager, private router:Router) { }


  Register(){
  console.log(this.model)

  this.api.requestData('adminapi/register', this.model).subscribe(Response => {
    if(Response.status)
    {
      this.toastr.successToastr(Response.Message, 'Success!');
      this.router.navigate(['/login']);
    }
    else{
      this.toastr.errorToastr(Response.Message, 'Oops')
    }
  })
  }
}
