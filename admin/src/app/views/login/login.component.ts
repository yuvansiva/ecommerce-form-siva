import { Component } from '@angular/core';
import { MainService } from '../service/main.service';
import { ToastrManager } from 'ng6-toastr-notifications';
import { Router } from '@angular/router';


@Component({
  selector: 'app-dashboard',
  templateUrl: 'login.component.html'
})
export class LoginComponent {

  model: any = {}
  constructor( private api: MainService, private toastr: ToastrManager, private router:Router){}


  Login(){
    this.api.requestData('adminapi/login', this.model).subscribe(Response => {
      if(Response.status)
      {
        this.toastr.successToastr(Response.Message, 'Success!');
        this.router.navigate(['/base/forms'])
      }
      else{
        this.toastr.errorToastr(Response.Message, 'Oops')
      }
    })
  }
 }



