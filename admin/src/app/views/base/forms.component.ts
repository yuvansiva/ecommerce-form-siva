import { Component } from '@angular/core';
import { MainService } from '../service/main.service';
import { ToastrManager } from 'ng6-toastr-notifications';
import { NgForm } from '@angular/forms';


@Component({
  templateUrl: 'forms.component.html'
})
export class FormsComponent {
   model : any = {};
   excelArray =[];
   products  = []
   _id: any
   show:boolean = true
  constructor(  private api: MainService, private toastr: ToastrManager) {
    this. getproduct();
   }

  isCollapsed: boolean = false;
  iconCollapse: string = 'icon-arrow-up';

  collapsed(event: any): void {
    // console.log(event);
  }

  expanded(event: any): void {
    // console.log(event);
  }

  toggleCollapse(): void {
    this.isCollapsed = !this.isCollapsed;
    this.iconCollapse = this.isCollapsed ? 'icon-arrow-down' : 'icon-arrow-up';
  }

  imageuploadprofFormat = false;
  fileImage: boolean = true;
  filepath;
  url1;
  image; imagePath; filename;
  filesToUpload: Array<File> = [];

  fileChangeEvent(fileInput: any) {
    if (fileInput.target.files && fileInput.target.files[0]) {
      var reader = new FileReader();
      this.imagePath = fileInput.target.files;
      reader.readAsDataURL(fileInput.target.files[0]);
      reader.onload = (event) => {
        this.url1 = reader.result;
      }
    }
    this.imageuploadprofFormat = false;
    var path = fileInput.target.files[0].type;
    this.filepath = path;
    if (path == "image/jpeg" || path == "image/gif" || path == "image/jpg" || path == "image/png") {
      this.filesToUpload = <Array<File>>fileInput.target.files;
      this.filename = fileInput.target.files[0].name;
    }
    else {
      this.toastr.errorToastr('Please choose a valid Image', 'Oops!');
      this.filesToUpload = [];
      this.filename = '';
    }
  }
  
  edit(data){
    this._id = data;
    console.log(this._id)
    var obj = {
      _id: data
    }
    console.log(obj)
  this.show = false
  this.api.requestData('adminapi/edit', obj).subscribe(Response => {
    console.log(Response)
    // this._id = Response.Message._id
    
    this.model.prdctname = Response.Message[0].prdctname;
    this.model.price = Response.Message[0].price;
    this.model.image = Response.Message[0].image
  })
}

  submit(m:NgForm){
    console.log(this.model)

      const formData: any = new FormData();
      const files: Array<File> = this.filesToUpload;
      if (files.length > 0) {
        for (let i = 0; i < files.length; i++) {
          formData.append("uploads[]", files[i], files[i]['name']);
        }
             console.log(formData)
        this.api.requestDataImage('adminapi/upload', formData).then(resData => {
          if (resData.status) {
            //  this.toastr.errorToastr(resData.Message, 'oops')
            
            var obj = {
              _id:this._id,
              prdctname: this.model.prdctname,
              price: this.model.price,
              image: resData.result
            }
            console.log(obj)
            this.api.requestData('adminapi/addproduct', obj).subscribe(resData => {
              if (resData.status) {
                this.toastr.successToastr(resData.Message, 'Success!');
                m.resetForm();
                this.filename = '';
                this.url1 = '';
                this.show = true
                this.getproduct();

              } else {
                this.toastr.errorToastr(resData.Message, 'Oops!');
              }
            })
          }
        })
      }
  
    
  }
  add()
  {

  this.getproduct();
  this.show = false
  // this._id = '';

  this._id = '';
  this.model.prdctname =''
  this.model.price = ''
  this.model.image = ''
  }

 getproduct(){
   this.api.getData('adminapi/getproduct').subscribe(Response => {
     console.log(Response)
     this.products = Response.Message;
     this.model._id = Response.Message._id;

   })
 }



delete(data){
  var obj = {
    _id: data
  }
  this.api.requestData('adminapi/delete', obj).subscribe(Response => {
    if(Response.status){
      this.toastr.successToastr(Response.Message, 'Success');
      // this.products =[];
      this.getproduct();
    }
    else{
      this.toastr.errorToastr(Response.Message, 'Oops')
    }
  
  })
}
back(){
  this.show = true
}


}
