import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions, Response } from '@angular/http';
import { throwError } from 'rxjs';
import { Observable } from 'rxjs/Observable';

import { environment } from '../../../environments/environment';


import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

@Injectable({
  providedIn: 'root'
})
export class MainService {

  headers: Headers;
  serviceHost = environment.Backendhost;
  options: RequestOptions;
  

  constructor(private http: Http) { }


  requestData(url, values): Observable<any> {

    return this.http
        .post(this.serviceHost + url, values, this.options)
        .map(this.extractData)
        .catch(this.handleError);
}




requestDataImage(url, formData) {
  return this.http.post(this.serviceHost + url, formData).toPromise().then(this.extractData).catch(this.handleError);
}

getData(url): Observable<any> {
  return this.http
      .get(this.serviceHost + url, this.options)
      .map(this.extractData)
      .catch(this.handleError);
}

private extractData(res: Response) {
  const body = res.json();
  return body || {};

}

private handleError(error: any) {
  const errMsg = (error.message) ? error.message :
      error.status ? `${error.status} - ${error.statusText}` : 'Server error';
  return throwError(errMsg);
}

getData1(url): Observable<any> {
  return this.http
      .get(url)
      .map(this.extractData)
      .catch(this.handleError);
}
}
