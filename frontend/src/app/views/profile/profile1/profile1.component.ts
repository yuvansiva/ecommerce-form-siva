import { Component, OnInit } from '@angular/core';
import {MainService} from '../../service/main.service'

@Component({
  selector: 'app-profile1',
  templateUrl: './profile1.component.html',
  styleUrls: ['./profile1.component.scss']
})
export class Profile1Component implements OnInit {
  
  productss : any = []
  image;
  prdctname ; price
  constructor(private api:MainService) {
    this.getproduct();
   }

  ngOnInit() {
  }

getproduct(){
  this.api.getData('adminapi/getproducts').subscribe(Response => {
    console.log(Response)
    this.productss = Response.Message
    console.log(this.productss)
    this.image = Response.Message[0].image;
    this.price = Response.Message[0].price
    this.prdctname = Response.Message[0].prdctname


    if(Response.status){
      
    }
  })
}

}
